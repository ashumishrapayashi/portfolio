export const blogData = [
    {
        id: 1,
        title: 'Planning ML Projects: Designing Architecture',
        description: 'Guidance on project planning and architectural design for machine learning initiatives',
        date: '',
        image: 'https://bera-group.com/wp-content/uploads/2020/07/Guide-to-Machine-Learning-and-AI.jpg',
        url: ''
    },
   
    {
        id: 5,
        title: 'APIs and Security Measures: Ensuring Protection',
        description: 'Exploration of APIs and security protocols to safeguard digital assets effectively.',
        date: '',
        image: 'https://tse2.mm.bing.net/th?id=OIP.zhMz8oe9N1KpyiwRtShTYAHaHZ&pid=Api&P=0&h=180',
        url: ''
    },
    {
        id: 3,
        title: 'BEING ANONYMOUS: A BEGINNERS GUIDE',
        description: 'Here are some basic ways to lay off the Radar',
        date: 'Aug 14, 2020',
        image: 'https://1.bp.blogspot.com/-GR19yCNbKCE/XuRcKEfKvhI/AAAAAAAAAwY/srFWw5pOpzMibeJb__SmgZpx9dIpNNWqgCK4BGAsYHg/w640-h426/anonymous1.jpg',
        url: 'https://hackzism.blogspot.com/2020/06/being-anonymous-beginners-guide.html'
    },
    {
        id: 2,
        title: 'CHECK WEATHER FROM TERMINAL USING WTTR.IN',
        description: 'wttr.in is a console-oriented weather forecast service that supports various information representation methods like ANSI-sequences for console HTTP clients, HTML, or PNG.',
        date: 'Aug 11, 2020',
        image: 'https://1.bp.blogspot.com/-OW7jX57tea4/XvnGxuEOslI/AAAAAAAABW0/R8lVT1AXDSwnvE0EGA9Ra49-LDm1ACwDgCK4BGAsYHg/s1216/wttr1.png',
        url: 'https://hackzism.blogspot.com/2020/06/check-weather-from-terminal-using-wttrin.html'
    },
   
    {
        id: 4,
        title: 'Understanding Domain/Cloud Service Provider Operations',
        description: 'Insight into the fundamental operations of domain and cloud service providers.',
        date: '',
        image: 'https://i.pinimg.com/originals/fd/84/b6/fd84b6ad05648408ede7542d4c88ce7e.jpg',
        url: ''
    },
    

    
]




// Do not remove any fields.
// Leave it blank instead as shown below.


/* 
{
    id: 1,
    title: 'Car Pooling System',
    description: '',
    date: 'Oct 1, 2020',
    image: '',
    url: 'https://preview.colorlib.com/theme/rezume/'
}, 
*/