import resume from '../assets/pdf/resume.pdf';
import profileImg from '../assets/png/Refferal.png'

export const headerData = {
    name: 'Ashutosh Mishra',
    title: "Full-Stack Developer | AI & ML",
    desciption:"A diligent Computer Science graduate with strong development and troubleshooting skills. Proficient in translating business requirements into technical solutions. ",
    image: profileImg,
    resumePdf: resume
}
