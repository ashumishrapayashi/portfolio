/* eslint-disable */
import girl1 from '../assets/svg/testimonials/girl1.svg'
import girl2 from '../assets/svg/testimonials/girl2.svg'
import girl3 from '../assets/svg/testimonials/girl3.svg'
import boy1 from '../assets/svg/testimonials/boy1.svg'
import boy2 from '../assets/svg/testimonials/boy2.svg'
import boy3 from '../assets/svg/testimonials/boy3.svg'



export const testimonialsData = [
    {
        id: 1,
        name: 'Dynamic Field Form',
        title: 'Github',
        text: "Form builder dynamically drags and drops various data type fields, validates with 1 to 500 field constraints, generates embeddable HTML.",
        image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/2300px-React-icon.svg.png' // linked
    },
    {
        id: 2,
        name: 'Program Wrapper',
        title: 'Github',
        text: "Wrapper of a program, monitors execution, auto-fixes issues or skip, logs time, schedular based supports re-execution.",
        image: 'https://upload.wikimedia.org/wikipedia/commons/c/c3/Python-logo-notext.svg'
    },
    {
        id: 3,
        name: 'Build Your Dependency',
        title: 'Github',
        text: "I've created a script automating custom module creation in Python, Java, and Node.js, enhancing code reusability and speeding execution.",
        image: 'https://wikidev.in/images/cli.png'
    },
    {
        id: 4,
        name: "To be added soon",
        title: 'Github',
        text: "There's plenty more code craft to come. I'll continue adding it as it gets productized.",
        image: boy1
    }
]

