
export const aboutData = {
    title: "Who I am",
    description1: "Exploring the limitless possibilities of technology, driven by the synergy of full stack mastery and AI innovation. With industry experience, I'm eager for growth.",
    description2: "My mission is to continue pushing the boundaries of technology, constantly seeking new challenges and opportunities for growth. I am committed to staying at the forefront of innovation, embracing emerging technologies, and delivering exceptional results for my clients and collaborators.",
    image: 1
}