export const achievementData = {
    bio : "Putting quantified achievements on a resume is great, but I just don’t work with hard numbers. I have no data to support the quality of my work. If you have lots of relevant experience, the heading statement of your resume will be a summary.",
    achievements : [
        {
            id : 1,
            title : 'AWS Certified Machine Learning – Specialty',
            details : 'Elevates expertise, validates skills in machine learning development.',
            date : 'Aug 20, 2023',
            field : 'Automation',
            image : 'https://www.surepassexam.com/template/default/surepassexam.com/media/images/cert/aws-certified-machine-learning-specialty.png'
        },
        {
            id : 2,
            title : 'IBM Full Stack Software Developer Professional Certificate',
            details : 'Comprehensive training for mastering full stack development',
            date : 'March 10, 2023',
            field : 'Automation',
            image : 'https://logosmarcas.net/wp-content/uploads/2020/09/IBM-Logo-1967-1972.png'
        },
        {
            id : 3,
            title : 'Cryptography and network security',
            details : 'By National Programme on Technology Enhanced Learning (NPTEL).',
            date : 'july 02, 2020',
            field : 'Automation',
            image : 'https://www.logolynx.com/images/logolynx/be/bed8f1a279a0882c1d7044f1e6c821f1.png'
        },
       

    ]
}


// Do not remove any fields.
// Leave it blank instead as shown below.

/*

export const achievementData = {
    bio : "",
    achievements : []
}

*/