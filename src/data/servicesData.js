/* eslint-disable */
import { BiShoppingBag, BiPencil, BiBug } from "react-icons/bi";
import { BsCodeSlash, BsClipboardData } from "react-icons/bs";
import { AiOutlineMail, AiFillAudio } from "react-icons/ai";
import { DiDatabase, DiAptana } from "react-icons/di";
import { IoIosApps } from "react-icons/io";
import { MdDeveloperMode } from "react-icons/md";
import { DiIllustrator } from "react-icons/di";


export const servicesData = [
    {
        id: 1,
        title: 'Frontend Development',
        icon: <BsCodeSlash />
    },
    {
        id: 2,
        title: 'Backend Development',
        icon: <MdDeveloperMode />
    },
    {
        id: 3,
        title: 'Bug Buster',
        icon: <BiBug />
    },
    // {
    //     id: 4,
    //     title: 'Internet Research',
    //     icon: <FaInternetExplorer />
    // },
    {
         id: 5,
         title: 'Custom API Development',
         icon: <IoIosApps />
     },

   
    {
        id: 7,
        title: 'AI & ML Models',
        icon: <DiIllustrator />
    },
    // {
    //     id: 8,
    //     title: 'Pinterest Virtual Assistant',
    //     icon: <FaPinterest />
    // }, 
    // {
    //     id: 10,
    //     title: 'Data Entry', 
    //     icon: <BsClipboardData />
    // },
    // {
    //     id: 12,
    //     title: 'Audio Transcription',
    //     icon: <AiFillAudio />
    // },

]
