import one from '../assets/svg/projects/one.svg'
// import two from '../assets/svg/projects/two.svg'
import three from '../assets/svg/projects/three.svg'
// import four from '../assets/svg/projects/four.svg'
import five from '../assets/svg/projects/five.svg'
// import six from '../assets/svg/projects/six.svg'
import seven from '../assets/svg/projects/seven.svg'
// import eight from '../assets/svg/projects/eight.svg'


export const projectsData = [
    {
        id: 1,
        projectName: 'A/B Desk',
        projectDesc: 'This project aims to build an accounts and bills management system that consolidates all information about products, invoices and inventory data.',
        tags: ['Node.js', 'Express.js', 'MongoDb', 'React.js'],
        code: 'https://gitlab.com/ashumishrapayashi/accontDesk',
        demo: 'https://gitlab.com/ashumishrapayashi/accontDesk/-/blob/main/README.md?ref_type=heads',
        image: one
    },
   
    {
        id: 3,
        projectName: 'Vedic',
        projectDesc: 'This project aims to build a disease prediction system based on 5-6 symtoms/',
        tags: ['Flask', 'Machine Learning'],
        code: 'https://gitlab.com/ashumishrapayashi/Vedic',
        demo: 'https://gitlab.com/ashumishrapayashi/Vedic/-/blob/main/README.md?ref_type=heads',
        image: 'https://www.svgrepo.com/show/147416/doctor.svg'
    },
    {
        id: 7,
        projectName: 'Freemex-master',
        projectDesc: 'Freemax online trading game which allows you to buy and sell stocks at your convenience and at the current market price. The player at the end with highest total stocks and cash wins the game.',
        tags: ['Django', 'HTML', 'CSS', 'JS'],
        code: 'https://gitlab.com/ashumishrapayashi/freemex',
        demo: 'https://gitlab.com/ashumishrapayashi/testgit/-/blob/main/README.md?ref_type=heads',
        image: seven
    },
    {
        id: 2,
        projectName: 'Ajax-File',
        projectDesc: 'This project is a file uploader system that allows users to upload more then 20 extention data file and then it will parse that file in a appropriate JSON,CSV,XML dataformat.',
        tags: ['Django', 'Tailwind CSS'],
        code: 'https://gitlab.com/ashumishrapayashi/ajax-file',
        demo: 'https://gitlab.com/ashumishrapayashi/ajax-file/-/blob/main/README.md?ref_type=heads',
        image: three
    },
    // {
    //     id: 4,
    //     projectName: 'Android Patient Tracker',
    //     projectDesc: 'This project involves the development of an Android application for viewing and managing patient data.',
    //     tags: ['Flutter', 'Firebase'],
    //     code: 'https://github.com/hhhrrrttt222111/developer-portfolio',
    //     demo: 'https://github.com/hhhrrrttt222111/developer-portfolio',
    //     image: four
    // },
    {
        id: 5,
        projectName: 'FimoDuck',
        projectDesc:'"A food delivery platform for multiple vendors to list restaurants, get approval, and add food products. Includes vendor and client control panels."',
        tags: ['React', 'Node JS', 'GraphQL', 'Rest API'],
        code: 'https://gitlab.com/ashumishrapayashi/fimoduck',
        demo: 'https://gitlab.com/ashumishrapayashi/fimoduck/-/blob/main/README.md?ref_type=heads',
        image: 'https://www.shift4shop.com/2015/images/industries/food-beverages/food-beverages.png'
    },
    {
        id: 6,
        projectName: 'FimoDuck Driver',
        projectDesc:"This module extends FimoDuck, a food delivery platform, with web and mobile apps for delivery personnel to manage orders and earnings.",
        tags: ['React', 'Node JS'],
        code: 'https://gitlab.com/ashumishrapayashi/fimoduck',
        demo: 'https://gitlab.com/ashumishrapayashi/fimoduck/-/blob/main/README.md?ref_type=heads',
        image: five
    },
    {
        id: 7,
        projectName: 'Freemex-master',
        projectDesc: 'Freemax online trading game which allows you to buy and sell stocks at your convenience and at the current market price. The player at the end with highest total stocks and cash wins the game.',
        tags: ['Django', 'HTML', 'CSS', 'JS'],
        code: 'https://gitlab.com/ashumishrapayashi/freemex',
        demo: 'https://gitlab.com/ashumishrapayashi/testgit/-/blob/main/README.md?ref_type=heads',
        image: seven
    },
    {
        id: 8,
        projectName: 'Bet-Haggle',
        projectDesc: 'A proof of transection based project which stores and manage NFT of invoice data in to the blockchain.',
        tags: ['Solidity', 'Node JS'],
        code: 'https://gitlab.com/ashumishrapayashi/bet-haggle',
        demo: 'https://gitlab.com/ashumishrapayashi/bet-haggle/-/blob/main/README.md?ref_type=heads',
        image: 'https://pic.onlinewebfonts.com/svg/img_83039.svg'
    },
]


// Do not remove any fields.
// Leave it blank instead as shown below

/* 
{
    id: 1,
    projectName: 'Car Pooling System',
    projectDesc: '',
    tags: ['Flutter', 'React'],
    code: '',
    demo: '',
    image: ''
}, 
*/