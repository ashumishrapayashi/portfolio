from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/submit-contact-form', methods=['POST'])
def submit_contact_form():
    try:
        data = request.json
        # Process the form data (e.g., save it to a Google Sheet)
        # Replace this with your actual processing logic
        print("Received form data:", data)
        # Return success response
        return jsonify({'status': 'success'}), 200
    except Exception as e:
        # Return error response
        return jsonify({'error': str(e)}), 500

if __name__ == '__main__':
    app.run(debug=True)
